---
# ReactJS - Advanced Template (Configured)
###### A template that runs/builds on a self-configured working environment 

## Bundler:
### Webpack:
`webpack`
`webpack --watch`
`webpack-dev-server`

## Transpiler:
### Babel: 
`babel-core`
`babel-loader`
`babel-preset-es2015`
`babel-preset-react`

## Development Dependencies
### Babel Polyfill:
`babel-polyfill`
### Redux Dev Tools
`redux-devtools`

## Dependencies
### React Router:
`react-router`
### Redux:
`redux`
### React Redux:
`react-redux`
### Redux Thunk
`redux-thunk`
### React Thunk
`react-thunk`
### Redux Logger
`redux-logger`
### React Bootstrap
`react-bootstrap`
### Bootstrap 3
`bootstrap@3`
---