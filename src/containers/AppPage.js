import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'

class AppPage extends Component {
	render() {
		return (
			<div>
				<p>Hello World!</p>
			</div>
		)
	}
}

AppPage.propTypes = {
}

function mapStateToProps(state) {
	return {
	}
}

export default connect(mapStateToProps)(AppPage)