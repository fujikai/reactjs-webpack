var webpack = require("webpack");
var path = require("path");

var DEV_PATH = path.resolve(__dirname, "./src");
var PROD_PATH = path.resolve(__dirname, "./public/scripts");

var config = {
	entry: DEV_PATH + "/" + "index.js",
	output: {
		path: PROD_PATH,
		filename: "main.js"
	},
	devServer: {
		inline: true,
		port: 8080
	},
	module: {
		loaders: [
			{
				include: DEV_PATH,
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel',
				query: {
					presets: ['es2015', 'react']
				}
			}
		]
	}
}

module.exports = config;
